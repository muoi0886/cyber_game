-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 30, 2022 at 09:59 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cyber_game`
--

-- --------------------------------------------------------

--
-- Table structure for table `bill`
--

CREATE TABLE `bill` (
  `id` int(11) NOT NULL,
  `guest_id` int(11) DEFAULT NULL,
  `device_id` int(11) DEFAULT NULL,
  `sale_id` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `time_play` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `create_by` int(11) DEFAULT 1,
  `create_at` datetime DEFAULT current_timestamp(),
  `update_by` int(11) DEFAULT NULL,
  `update_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bill`
--

INSERT INTO `bill` (`id`, `guest_id`, `device_id`, `sale_id`, `amount`, `time_play`, `status`, `create_by`, `create_at`, `update_by`, `update_at`) VALUES
(26, 0, 2, 0, 30000, 1, 1, 1, '2022-03-30 14:46:00', NULL, '2022-03-30 14:56:23'),
(27, 0, 0, 0, 30000, 1, 1, 1, '2022-03-30 14:46:45', NULL, '2022-03-30 14:55:33'),
(28, 9, 29, 0, 10000, 1, 1, 1, '2022-03-30 14:47:13', NULL, NULL),
(29, 10, 37, 0, 60000, 5, 1, 1, '2022-03-30 14:48:21', NULL, '2022-03-30 14:57:45'),
(30, 0, 0, 0, 10000, 1, 1, 1, '2022-03-30 14:55:30', NULL, NULL),
(31, 0, 0, 0, 120000, 12, 1, 1, '2022-03-30 14:57:03', NULL, '2022-03-30 14:57:08'),
(32, 0, 0, 0, 120000, 12, 1, 1, '2022-03-30 14:57:10', NULL, NULL),
(33, 9, 37, 0, 120000, 12, 1, 1, '2022-03-30 14:57:14', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bill_food`
--

CREATE TABLE `bill_food` (
  `id` int(11) NOT NULL,
  `bill_id` int(11) DEFAULT NULL,
  `food_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `create_by` int(11) DEFAULT 1,
  `create_at` datetime DEFAULT current_timestamp(),
  `update_by` int(11) DEFAULT NULL,
  `update_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bill_food`
--

INSERT INTO `bill_food` (`id`, `bill_id`, `food_id`, `qty`, `status`, `create_by`, `create_at`, `update_by`, `update_at`) VALUES
(15, 26, 5, 2, 1, 1, '2022-03-30 14:46:18', NULL, NULL),
(16, 26, 8, 2, 1, 1, '2022-03-30 14:46:58', NULL, NULL),
(17, 27, 8, 2, 1, 1, '2022-03-30 14:47:05', NULL, NULL),
(18, 29, 5, 1, 1, 1, '2022-03-30 14:57:45', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `catalog`
--

CREATE TABLE `catalog` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `create_by` int(11) DEFAULT 1,
  `create_at` datetime DEFAULT current_timestamp(),
  `update_by` int(11) DEFAULT NULL,
  `update_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `catalog`
--

INSERT INTO `catalog` (`id`, `name`, `status`, `create_by`, `create_at`, `update_by`, `update_at`) VALUES
(5, 'may tinh', 1, 1, '2022-03-30 14:10:48', NULL, NULL),
(6, 'phu kien', 1, 1, '2022-03-30 14:11:28', NULL, NULL),
(7, 'vip', 1, 1, '2022-03-30 14:11:34', NULL, NULL),
(8, 'do an', 1, 1, '2022-03-30 14:11:44', NULL, NULL),
(9, 'do uong', 1, 1, '2022-03-30 14:11:49', NULL, NULL),
(10, 'thuong', 1, 1, '2022-03-30 14:25:20', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `detail_devices`
--

CREATE TABLE `detail_devices` (
  `id` int(11) NOT NULL,
  `cpu` varchar(255) DEFAULT NULL,
  `ram` varchar(255) DEFAULT NULL,
  `vga` varchar(255) DEFAULT NULL,
  `psu` varchar(255) DEFAULT NULL,
  `ssd` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `create_by` int(11) DEFAULT 1,
  `create_at` datetime DEFAULT current_timestamp(),
  `update_by` int(11) DEFAULT NULL,
  `update_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `detail_devices`
--

INSERT INTO `detail_devices` (`id`, `cpu`, `ram`, `vga`, `psu`, `ssd`, `status`, `create_by`, `create_at`, `update_by`, `update_at`) VALUES
(27, 'i5', '8gb', '1050', '65w', '256', 1, 1, '2022-03-30 14:13:37', NULL, NULL),
(28, 'ryzen 5', '8gb', '3060', '120w', '512', 1, 1, '2022-03-30 14:14:25', NULL, NULL),
(29, 'i7', '16gb', '1060', '80w', '512', 1, 1, '2022-03-30 14:14:49', NULL, NULL),
(30, 'i5', '16gb', '1060', '80w', '512', 1, 1, '2022-03-30 14:14:55', NULL, NULL),
(32, 'i7', '16gb', '1060', '80w', '512', 0, 1, '2022-03-30 14:16:07', NULL, '2022-03-30 14:16:31'),
(34, 'i7', '16gb', '1060', '80w', '512', 0, 1, '2022-03-30 14:16:44', NULL, '2022-03-30 14:17:00'),
(35, 'i7', '16gb', '1060', '80w', '512', 1, 1, '2022-03-30 14:17:11', NULL, NULL),
(36, 'i7', '16gb', '1060', '80w', '512', 1, 1, '2022-03-30 14:17:19', NULL, NULL),
(37, 'i7', '16gb', '1060', '80w', '512', 1, 1, '2022-03-30 14:17:26', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--

CREATE TABLE `devices` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `catalog_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `create_by` int(11) DEFAULT 1,
  `create_at` datetime DEFAULT current_timestamp(),
  `update_by` int(11) DEFAULT NULL,
  `update_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `devices`
--

INSERT INTO `devices` (`id`, `name`, `catalog_id`, `status`, `create_by`, `create_at`, `update_by`, `update_at`) VALUES
(27, 'may01', 5, 1, 1, '2022-03-30 14:13:10', NULL, NULL),
(28, 'may02', 5, 1, 1, '2022-03-30 14:13:45', NULL, NULL),
(29, 'may03', 5, 1, 1, '2022-03-30 14:13:49', NULL, NULL),
(30, 'may04', 5, 1, 1, '2022-03-30 14:13:51', NULL, NULL),
(31, 'chuot razer', 6, 0, 1, '2022-03-30 14:15:15', NULL, '2022-03-30 14:15:51'),
(32, 'ban phim razer', 6, 0, 1, '2022-03-30 14:15:26', NULL, '2022-03-30 14:15:54'),
(33, 'man hinh', 6, 0, 1, '2022-03-30 14:15:36', NULL, '2022-03-30 14:15:53'),
(34, 'may07', 6, 0, 1, '2022-03-30 14:16:38', NULL, '2022-03-30 14:16:59'),
(35, 'may05', 5, 1, 1, '2022-03-30 14:17:07', NULL, '2022-03-30 14:18:00'),
(36, 'may06', 5, 1, 1, '2022-03-30 14:17:15', NULL, '2022-03-30 14:18:02'),
(37, 'may08', 5, 1, 1, '2022-03-30 14:17:23', NULL, '2022-03-30 14:17:58');

-- --------------------------------------------------------

--
-- Table structure for table `food`
--

CREATE TABLE `food` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `catalog_id` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `create_by` int(11) DEFAULT 1,
  `create_at` datetime DEFAULT current_timestamp(),
  `update_by` int(11) DEFAULT NULL,
  `update_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `food`
--

INSERT INTO `food` (`id`, `name`, `catalog_id`, `price`, `status`, `create_by`, `create_at`, `update_by`, `update_at`) VALUES
(5, 'mi tom ', 8, 10000, 1, 1, '2022-03-30 14:18:25', NULL, NULL),
(6, 'xuc xich', 8, 10000, 1, 1, '2022-03-30 14:18:40', NULL, NULL),
(7, 'banh mi', 8, 15000, 1, 1, '2022-03-30 14:18:52', NULL, NULL),
(8, 'coca', 9, 10000, 1, 1, '2022-03-30 14:19:02', NULL, NULL),
(9, 'tra da', 9, 3000, 1, 1, '2022-03-30 14:19:16', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `guest`
--

CREATE TABLE `guest` (
  `id` int(11) NOT NULL,
  `user` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `cmt` varchar(255) DEFAULT NULL,
  `catalog_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `create_by` int(11) DEFAULT 1,
  `create_at` datetime DEFAULT current_timestamp(),
  `update_by` int(11) DEFAULT NULL,
  `update_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `guest`
--

INSERT INTO `guest` (`id`, `user`, `password`, `name`, `phone`, `cmt`, `catalog_id`, `status`, `create_by`, `create_at`, `update_by`, `update_at`) VALUES
(9, 'muoimuoi', '1', 'lemuoi', '04432432432', '324324324324', 7, 1, 1, '2022-03-30 14:25:03', NULL, NULL),
(10, 'minhminh', '1', 'tuan minh', '043243243', '41432432432', 7, 1, 1, '2022-03-30 14:25:48', NULL, '2022-03-30 14:28:25'),
(11, 'gianggiang', '1', 'le giang', '432432432', '324324324234', 7, 0, 1, '2022-03-30 14:26:11', NULL, '2022-03-30 14:28:30'),
(12, 'gianggiang123', '1', 'le giang', '082124124', '41214214214', 7, 1, 1, '2022-03-30 14:29:24', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` int(11) NOT NULL,
  `amount` int(11) DEFAULT NULL,
  `create_by` int(11) DEFAULT 1,
  `create_at` datetime DEFAULT current_timestamp(),
  `update_by` int(11) DEFAULT NULL,
  `update_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `id` int(11) NOT NULL,
  `user` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `cmt` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `create_by` int(11) DEFAULT 1,
  `create_at` datetime DEFAULT current_timestamp(),
  `update_by` int(11) DEFAULT NULL,
  `update_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `user`, `password`, `name`, `phone`, `address`, `cmt`, `status`, `create_by`, `create_at`, `update_by`, `update_at`) VALUES
(11, 'gianggiang', '123', 'le giang', '042432432', 'ha noi', '0424124214', 1, 1, '2022-03-30 14:30:09', NULL, NULL),
(12, 'minh', '123', 'le minh', '042432432', 'ha noi', '0424124214', 1, 1, '2022-03-30 14:45:31', NULL, NULL),
(13, 'muoi123', '123', 'le muoi', '4346456546', 'ha noi', '32554354354', 1, 1, '2022-03-30 14:45:44', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bill`
--
ALTER TABLE `bill`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bill_food`
--
ALTER TABLE `bill_food`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `catalog`
--
ALTER TABLE `catalog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_devices`
--
ALTER TABLE `detail_devices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `food`
--
ALTER TABLE `food`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `guest`
--
ALTER TABLE `guest`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bill`
--
ALTER TABLE `bill`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `bill_food`
--
ALTER TABLE `bill_food`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `catalog`
--
ALTER TABLE `catalog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `devices`
--
ALTER TABLE `devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `food`
--
ALTER TABLE `food`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `guest`
--
ALTER TABLE `guest`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
