/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao;

import connect.ConnectHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Food;

/**
 *
 * @author lexuanmuoi
 */
public class FoodDao implements Dao{
    @Override
    public boolean insert(Object ob) throws SQLException {
        Food fd = new Food();
        fd = (Food) ob;
        String query = "INSERT INTO food(name, catalog_id, price) "
                + "values (?,?,?)";
        Connection conn = ConnectHelper.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        pstmt.setString(1, fd.getName());
        pstmt.setInt(2, fd.getCatalog());
        pstmt.setInt(3, fd.getPrice());
        return pstmt.execute();
    }

    @Override
    public List<Object> findAll() throws SQLException {
       
        List<Object> FoodList = new ArrayList<>();   
  
        String query = "SELECT * FROM food where status !=0";
        Connection conn = ConnectHelper.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        ResultSet rs = pstmt.executeQuery(query);
        while (rs.next()) {
            Food fd = new Food(rs.getInt("id"), rs.getString("name"),
                    rs.getInt("catalog_id"), rs.getInt("price")
                    , rs.getInt("status"));
            FoodList.add(fd);
        }
        return FoodList;

    }
public List<Food> search(String name) throws SQLException {
       
        List<Food> FoodList = new ArrayList<>();   
  
        String query = "SELECT * FROM food where status !=0 and name LIKE '%"+name+"%'";
        Connection conn = ConnectHelper.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        ResultSet rs = pstmt.executeQuery(query);
        while (rs.next()) {
            Food fd = new Food(rs.getInt("id"), rs.getString("name"),
                    rs.getInt("catalog_id"), rs.getInt("price")
                    , rs.getInt("status"));
            FoodList.add(fd);
        }
        return FoodList;

    }
    @Override
    public boolean update(Object ob) throws SQLException {
        Food fd = new Food();
        fd = (Food) ob;
        String query = "UPDATE food SET name=?,catalog_id=?, price=? "
                + "WHERE id='" + fd.getId() + "'";
        Connection conn = ConnectHelper.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        pstmt.setString(1, fd.getName());
        pstmt.setInt(2, fd.getCatalog());
        pstmt.setInt(3, fd.getPrice());
        return pstmt.execute();
    }

    @Override
    public boolean delete(Object ob) throws SQLException {
       Food fd = new Food();
        fd = (Food) ob;
        String query = "UPDATE food SET status = 0 "
                + "WHERE id='" + fd.getId() + "'";
        Connection conn = ConnectHelper.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        return pstmt.execute();
    }
}
