/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao;

import connect.ConnectHelper;
import java.awt.Component;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import model.Staff;

/**
 *
 * @author lexuanmuoi
 */
public class StaffDao implements Dao {

    public boolean checkInsert(String user) throws SQLException {
        Staff st = new Staff();
        boolean check = false;
        String query = "SELECT user FROM staff WHERE user LIKE '%" + user + "%' and status != 0";
        Connection conn = ConnectHelper.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        java.sql.ResultSet rs = pstmt.executeQuery(query);
        rs = pstmt.executeQuery(query);
        if (rs.next()) {
            check = true;
        }
        return check;
    }

    @Override
    public boolean insert(Object ob) throws SQLException {
        Staff st = new Staff();
        st = (Staff) ob;
        if (checkInsert(st.getUser()) == true) {
            Component rootPane = null;
            JOptionPane.showMessageDialog(rootPane, "user has been created");
        } else {
        String query = "INSERT INTO staff(user, password,name,phone,address,cmt)" + "values (?,?,?,?,?,?)";
        Connection conn = ConnectHelper.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        pstmt.setString(1, st.getUser());
        pstmt.setString(2, st.getPassword());
        pstmt.setString(3, st.getName());
        pstmt.setString(4, st.getPhone());
        pstmt.setString(5, st.getAddress());
        pstmt.setString(6, st.getCMT());
        return pstmt.execute();
        }
        return false;
    }

    @Override
    public List<Object> findAll() throws SQLException {
        List<Object> StaffList = new ArrayList<>();

        String query = "SELECT * FROM staff where status !=0";
        Connection conn = ConnectHelper.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        java.sql.ResultSet rs = pstmt.executeQuery(query);
        while (rs.next()) {
            Staff st = new Staff(rs.getInt("id"), rs.getString("user"),
                    rs.getString("password"), rs.getString("name"),
                    rs.getString("phone"), rs.getString("address"),
                    rs.getString("cmt"));
            StaffList.add(st);
        }
        return StaffList;
    }

    public List<Staff> search(String name) throws SQLException {
        List<Staff> StaffList = new ArrayList<>();

        String query = "SELECT * FROM staff where status !=0 and name LIKE '%" + name + "%'";
        Connection conn = ConnectHelper.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        java.sql.ResultSet rs = pstmt.executeQuery(query);
        while (rs.next()) {
            Staff st = new Staff(rs.getInt("id"), rs.getString("user"),
                    rs.getString("password"), rs.getString("name"),
                    rs.getString("phone"), rs.getString("address"),
                    rs.getString("cmt"));
            StaffList.add(st);
        }
        return StaffList;
    }

    @Override
    public boolean update(Object ob) throws SQLException {
        Staff st = new Staff();
        st = (Staff) ob;
        String query = "UPDATE staff SET user=?,password=?,name=?,phone=?,address=?,cmt=?"
                + " WHERE id='" + st.getId() + "'";
        Connection conn = ConnectHelper.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        pstmt.setString(1, st.getUser());
        pstmt.setString(2, st.getPassword());
        pstmt.setString(3, st.getName());
        pstmt.setString(4, st.getPhone());
        pstmt.setString(5, st.getAddress());
        pstmt.setString(6, st.getCMT());
        return pstmt.execute();
    }

    @Override
    public boolean delete(Object ob) throws SQLException {
        Staff st = new Staff();
        st = (Staff) ob;
        String query = "UPDATE staff SET status = 0 "
                + "WHERE id='" + st.getId() + "'";
        Connection conn = ConnectHelper.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        return pstmt.execute();
    }

}
