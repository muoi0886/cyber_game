/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao;

import connect.ConnectHelper;
import java.awt.Component;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import model.Guest;

/**
 *
 * @author lexuanmuoi
 */
public class GuestDao implements Dao {

    public boolean checkInsert(String user) throws SQLException {
        Guest gs = new Guest();
        boolean check = false;
        String query = "SELECT user FROM guest WHERE user LIKE '%" + user + "%' and status != 0";
        Connection conn = ConnectHelper.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        java.sql.ResultSet rs = pstmt.executeQuery(query);
        rs = pstmt.executeQuery(query);
        if (rs.next()) {
            check = true;
        }
        return check;
    }

    @Override
    public boolean insert(Object ob) throws SQLException {

        Guest gs = new Guest();
        gs = (Guest) ob;
        if (checkInsert(gs.getUser()) == true) {
            Component rootPane = null;
            JOptionPane.showMessageDialog(rootPane, "user has been created");
        } else {
            String query = "INSERT INTO guest(user, password,name,phone,cmt,catalog_id)" + "values (?,?,?,?,?,?)";
            Connection conn = ConnectHelper.getConnection();
            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setString(1, gs.getUser());
            pstmt.setString(2, gs.getPassword());
            pstmt.setString(3, gs.getName());
            pstmt.setString(4, gs.getPhone());
            pstmt.setString(5, gs.getCMT());
            pstmt.setInt(6, gs.getCatalogId());
           

            return pstmt.execute();
        }
        return false;
    }

    @Override
    public List<Object> findAll() throws SQLException {
        List<Object> GuestList = new ArrayList<>();

        String query = "SELECT * FROM guest where status !=0";
        Connection conn = ConnectHelper.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        java.sql.ResultSet rs = pstmt.executeQuery(query);
        while (rs.next()) {
            Guest gs = new Guest(rs.getInt("id"), rs.getString("user"),
                    rs.getString("password"), rs.getString("name"),
                    rs.getString("phone"), rs.getString("cmt"),
                    rs.getInt("catalog_id"));
            GuestList.add(gs);
        }
        return GuestList;
    }

    public List<Guest> search(String name) throws SQLException {
        List<Guest> GuestList = new ArrayList<>();

        String query = "SELECT * FROM guest where status !=0 and name LIKE '%" + name + "%'";
        Connection conn = ConnectHelper.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        java.sql.ResultSet rs = pstmt.executeQuery(query);
        while (rs.next()) {
            Guest gs = new Guest(rs.getInt("id"), rs.getString("user"),
                    rs.getString("password"), rs.getString("name"),
                    rs.getString("phone"), rs.getString("cmt"),
                    rs.getInt("catalog_id"));
            GuestList.add(gs);
        }
        return GuestList;
    }

    @Override
    public boolean update(Object ob) throws SQLException {
        Guest gs = new Guest();
        gs = (Guest) ob;
        String query = "UPDATE guest SET user=?,password=?,name=?,phone=?,cmt=?"
                + ",catalog_id=? WHERE id='" + gs.getId() + "'";
        Connection conn = ConnectHelper.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        pstmt.setString(1, gs.getUser());
        pstmt.setString(2, gs.getPassword());
        pstmt.setString(3, gs.getName());
        pstmt.setString(4, gs.getPhone());
        pstmt.setString(5, gs.getCMT());
        pstmt.setInt(6, gs.getCatalogId());
       
        return pstmt.execute();

    }

    @Override
    public boolean delete(Object ob) throws SQLException {
        Guest gs = new Guest();
        gs = (Guest) ob;
        String query = "UPDATE guest SET status = 0 "
                + "WHERE id='" + gs.getId() + "'";
        Connection conn = ConnectHelper.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        return pstmt.execute();
    }

}
