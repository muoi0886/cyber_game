/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao;

import connect.ConnectHelper;
import java.awt.Component;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import model.Device;
import model.DeviceComputer;

/**
 *
 * @author A
 */
public class DetailDeviceDao implements Dao{
    
    public boolean checkInsert(int id) throws SQLException {
        DeviceComputer dc = new DeviceComputer();
        boolean check = false;
        String query = "SELECT id FROM detail_devices WHERE id = '" + id + "'";
        Connection conn = ConnectHelper.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        java.sql.ResultSet rs = pstmt.executeQuery(query);
        rs = pstmt.executeQuery(query);
        if (rs.next()) {
            check = true;
        }
        return check;
    }
    
    @Override
    public boolean insert(Object ob) throws SQLException {
        DeviceComputer dvc = new DeviceComputer();
        dvc = (DeviceComputer) ob;
        if (checkInsert(dvc.getId()) == true) {
            Component rootPane = null;
            JOptionPane.showMessageDialog(rootPane, "device has been detail");
        } else {
        String query = "INSERT INTO detail_devices(id,cpu, ram, vga, psu, ssd) "
                + "values (?,?,?,?,?,?)";
        Connection conn = ConnectHelper.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        pstmt.setInt(1, dvc.getId());
        pstmt.setString(2, dvc.getCpu());
        pstmt.setString(3, dvc.getRam());
        pstmt.setString(4, dvc.getVga());
        pstmt.setString(5, dvc.getPsu());
        pstmt.setString(6, dvc.getSsd());
        return pstmt.execute();
        }
        return false;
    }

    @Override
    public List<Object> findAll() throws SQLException {
        
        List<Object> DeviceList = new ArrayList<>();
  
        String query = "SELECT * FROM detail_devices where status != 0";
        Connection conn = ConnectHelper.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        ResultSet rs = pstmt.executeQuery(query);
        while (rs.next()) {
            DeviceComputer dc = new DeviceComputer(rs.getInt("id"), 
                   rs.getString("cpu"),rs.getString("ram"), 
                   rs.getString("vga"), rs.getString("psu"), rs.getString("ssd")
                   ,rs.getInt("status"));
            DeviceList.add(dc);
        }
        return DeviceList;

    }
    public List<DeviceComputer> search(int id) throws SQLException {
        
        List<DeviceComputer> DeviceList = new ArrayList<>();
  
        String query = "SELECT * FROM detail_devices where status != 0 and id LIKE '%"+id+"%'";
        Connection conn = ConnectHelper.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        ResultSet rs = pstmt.executeQuery(query);
        while (rs.next()) {
            DeviceComputer dc = new DeviceComputer(rs.getInt("id"), 
                   rs.getString("cpu"),rs.getString("ram"), 
                   rs.getString("vga"), rs.getString("psu"), rs.getString("ssd")
                   ,rs.getInt("status"));
            DeviceList.add(dc);
        }
        return DeviceList;

    }

    @Override
    public boolean update(Object ob) throws SQLException {
        DeviceComputer dvc = new DeviceComputer();
        dvc = (DeviceComputer) ob;
        String query = "UPDATE detail_devices SET cpu=?,ram=?,vga=?,psu=?,ssd=? "
                + "WHERE id='" + dvc.getId() + "'";
        Connection conn = ConnectHelper.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        pstmt.setString(1, dvc.getCpu());
        pstmt.setString(2, dvc.getRam());
        pstmt.setString(3, dvc.getVga());
        pstmt.setString(4, dvc.getPsu());
        pstmt.setString(5, dvc.getSsd());
        return pstmt.execute();
    }

    @Override
    public boolean delete(Object ob) throws SQLException {
        DeviceComputer dvc = new DeviceComputer();
        dvc = (DeviceComputer) ob;
        String query = "UPDATE detail_devices SET status = 0 "
                + "WHERE id='" + dvc.getId() + "'";
        Connection conn = ConnectHelper.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        return pstmt.execute();
    }
}
