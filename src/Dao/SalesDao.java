/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao;

import connect.ConnectHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Sales;

/**
 *
 * @author lexuanmuoi
 */
public class SalesDao implements Dao {

    @Override
    public List<Object> findAll() throws SQLException {

        List<Object> DeviceList = new ArrayList<>();

        String query = "SELECT MONTH(create_at) as month,YEAR(create_at) as year, "
                + "SUM(amount) as amount FROM bill GROUP BY MONTH(create_at)";
        Connection conn = ConnectHelper.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        ResultSet rs = pstmt.executeQuery(query);
        while (rs.next()) {
            Sales s = new Sales(rs.getInt("month"), rs.getInt("year"),rs.getInt("amount"));
            DeviceList.add(s);
        }
        return DeviceList;

    }

    @Override
    public boolean update(Object ob) throws SQLException {
        Sales s = new Sales();
        s = (Sales) ob;
        String query = "UPDATE sales SET "
                + "amount=(SELECT SUM(amount) as amount "
                + "FROM bill"
                + "WHERE month(create_at) ='" + s.getMonth() + "'" + " "
                + "and year(create_at) = '" + s.getYear() + "'" + "and "
                + "id='" + s.getId() + "'" +")";
        Connection conn = ConnectHelper.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        return pstmt.execute();
    }
    
//    public String setAmount(int month, int year) throws SQLException {
//        Sales s = new Sales();
//        String s1 = "SELECT SUM(amount) FROM bill WHERE month(create_at) =? and year(create_at) =?";
//        Connection connection = ConnectHelper.getConnection();
//        PreparedStatement pstmt = connection.prepareStatement(s1);
//        pstmt.setInt(1, month);
//        pstmt.setInt(2, year);
//        ResultSet rs = pstmt.executeQuery();
//        String am = null;
//        if (rs.next()) {
//            am = rs.getString(1);
//        }
//
//        return am;
//    }
//    
//    @Override
//    public boolean insert(Object ob) throws SQLException {
//        Sales s = new Sales();
//        s = (Sales) ob;
//        String s2 = "INSERT INTO sales(amount) values(?)";
//        Connection connection = ConnectHelper.getConnection();
//        PreparedStatement pstmt1 = connection.prepareStatement(s2);
//        pstmt1.setInt(1, Integer.parseInt(setAmount(s.getMonth(),s.getYear())));
//        return pstmt1.execute();
//    }

    @Override
    public boolean delete(Object ob) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public boolean insert(Object ob) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
