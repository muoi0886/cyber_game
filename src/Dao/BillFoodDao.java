/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao;

import connect.ConnectHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.BillFood;
import model.Food;

/**
 *
 * @author lexuanmuoi
 */
public class BillFoodDao implements Dao  {

    @Override
    public boolean insert(Object ob) throws SQLException {
        BillFood bFood = new BillFood();
        bFood = (BillFood) ob;
        String query = "insert into bill_food(bill_id,food_id,qty) values(?,?,?)";
        Connection connection = ConnectHelper.getConnection();
            PreparedStatement pstmt = connection.prepareStatement(query);
            pstmt.setInt(1, bFood.getBillId());
            pstmt.setInt(2,bFood.getFoodId());
            pstmt.setInt(3, bFood.getQty());
          
        return pstmt.execute();
    }

    @Override
    public List<Object> findAll() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public boolean update(Object ob) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
     public static List<Food> findName() throws SQLException{
        List<Food>FoodNameList = new ArrayList<>();
        String query = "select * from food";
        Connection connection = ConnectHelper.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(query);
       
        while(rs.next()){
            Food fd = new Food(rs.getInt("id"),rs.getString("name"), rs.getInt("price"));
            FoodNameList.add(fd);
        }
        return FoodNameList;
    }

    @Override
    public boolean delete(Object ob) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
