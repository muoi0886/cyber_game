/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao;

import connect.ConnectHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.sql.ResultSet;

import model.Device;

/**
 *
 * @author lexuanmuoi
 */
public class DeviceDao implements Dao {

    @Override
    public boolean insert(Object ob) throws SQLException {
        Device dv = new Device();
        dv = (Device) ob;
        String query = "INSERT INTO devices(name, catalog_id) values (?,?)";
        Connection conn = ConnectHelper.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        pstmt.setString(1, dv.getName());
        pstmt.setInt(2, dv.getCatalog());
        return pstmt.execute();
    }

    @Override
    public List<Object> findAll() throws SQLException {
       
        List<Object> DeviceList = new ArrayList<>();   
  
        String query = "SELECT * FROM devices where status != 0";
        Connection conn = ConnectHelper.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        ResultSet rs = pstmt.executeQuery(query);
        while (rs.next()) {
            Device dv = new Device(rs.getInt("id"), rs.getString("name"),
                    rs.getInt("catalog_id"), rs.getInt("status"));
            DeviceList.add(dv);
        }
        return DeviceList;

    }
public List<Device> search(String name) throws SQLException {
       
        List<Device> DeviceList = new ArrayList<>();   
  
        String query = "SELECT * FROM devices where status != 0 and name like '%"+name+"%'";
        Connection conn = ConnectHelper.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        ResultSet rs = pstmt.executeQuery(query);
        while (rs.next()) {
            Device dv = new Device(rs.getInt("id"), rs.getString("name"),
                    rs.getInt("catalog_id"), rs.getInt("status"));
            DeviceList.add(dv);
        }
        return DeviceList;

    }
    @Override
    public boolean update(Object ob) throws SQLException {
        Device dv = new Device();
        dv = (Device) ob;
        String query = "UPDATE devices SET name=?,catalog_id=? "
                + "WHERE id='" + dv.getId() + "'";
        Connection conn = ConnectHelper.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        pstmt.setString(1, dv.getName());
        pstmt.setInt(2, dv.getCatalog());
        return pstmt.execute();
    }

    @Override
    public boolean delete(Object ob) throws SQLException {
        Device dv = new Device();
        dv = (Device) ob;
        String query = "UPDATE devices SET status = 0 "
                + "WHERE id='" + dv.getId() + "'";
        Connection conn = ConnectHelper.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        return pstmt.execute();
    }

}
