/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao;

import connect.ConnectHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Bill;

/**
 *
 * @author lexuanmuoi
 */
public class BillDao implements Dao {

    @Override
    public boolean insert(Object ob) throws SQLException {
        Bill bill = new Bill();
        bill = (Bill) ob;
        String query = "insert into bill(guest_id,device_id,sale_id"
                + ",amount,time_play)values(?,?,?,?,?)";
        Connection connection = ConnectHelper.getConnection();
            PreparedStatement pstmt = connection.prepareStatement(query);
            pstmt.setInt(1, bill.getGuestId());
            pstmt.setInt(2,bill.getDeviceId());
            pstmt.setInt(3,bill.getSaleId());
            pstmt.setInt(4, bill.getAmount());
            pstmt.setInt(5, bill.getTimePlay());
        return pstmt.execute();
    }

    @Override
    public List<Object> findAll() throws SQLException {
       List<Object>ObjectList = new ArrayList<>();
//            List<Catalog>CatalogList = new ArrayList<>();
////        (List<Object>)(Catalog) Objectlist;
//        List<Catalog> CatalogList = (List<Catalog>)(List<Object>);
//        String query = "select * from bill";
        String query = " SELECT bill.id,guest.name, devices.name,bill.amount,bill.time_play,bill.status FROM bill,guest,devices"
                + " WHERE bill.guest_id = guest.id AND bill.device_id = devices.id and bill.status != 0;";
        Connection connection = ConnectHelper.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(query);
       
        while(rs.next()){
        Bill bill = new Bill(rs.getInt("id"), rs.getString("guest.name"), rs.getString("devices.name"),
        rs.getInt("amount"),rs.getInt("time_play"), rs.getInt("status"));
//        CatalogList.add(ct);
        ObjectList.add(bill);
//        List<A> a = getListOfA();

//    ObjectList.addAll(CatalogList);
        }
        return ObjectList;
    }

    @Override
    public boolean update(Object ob) throws SQLException {
        Bill bill = new Bill();
        bill = (Bill) ob;
        String query = "update bill set guest_id = ?,device_id = ? where id = "+bill.getId();
        Connection connection = ConnectHelper.getConnection();
            PreparedStatement pstmt = connection.prepareStatement(query);
            pstmt.setInt(1, bill.getGuestId());
            pstmt.setInt(2,bill.getDeviceId());
        return pstmt.execute();
    }
    public boolean updateAmount(Bill bill) throws SQLException {
 
        String query = "UPDATE bill SET amount=? "
                + "WHERE id='" + bill.getId() + "'";
        Connection conn = ConnectHelper.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        pstmt.setInt(1, bill.getAmount());
  
        return pstmt.execute();
    }
    
     public List<Bill> search(String name) throws SQLException {
       List<Bill>BillList = new ArrayList<>();

        String query = " SELECT bill.id,guest.name, devices.name,bill.amount,bill.time_play,bill.status FROM bill,guest,devices"
                + " WHERE bill.guest_id = guest.id AND bill.device_id = devices.id and bill.status != 0 and guest.name like '%"+name+"%';";
        Connection connection = ConnectHelper.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(query);
       
        while(rs.next()){
        Bill bill = new Bill(rs.getInt("id"), rs.getString("guest.name"), rs.getString("devices.name"),
        rs.getInt("amount"),rs.getInt("time_play"), rs.getInt("status"));
//        CatalogList.add(ct);
        BillList.add(bill);
//        List<A> a = getListOfA();

//    ObjectList.addAll(CatalogList);
        }
        return BillList;
    }

    @Override
    public boolean delete(Object ob) throws SQLException {
         Bill bill = new Bill();
        bill = (Bill) ob;
        String query = "update bill set status = 0 where id = "+bill.getId();
        Connection connection = ConnectHelper.getConnection();
            PreparedStatement pstmt = connection.prepareStatement(query);
        return pstmt.execute();
    }
}
