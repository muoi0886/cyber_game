/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao;

import connect.ConnectHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Catalog;
/**
 *
 * @author lexuanmuoi
 */
public class CatalogDao implements Dao {

    @Override
    public boolean insert(Object ob) throws SQLException {
        Catalog ct = new Catalog();
        ct = (Catalog) ob;
        String query = "insert into catalog(name) values(?)";
        Connection connection = ConnectHelper.getConnection();
            PreparedStatement pstmt = connection.prepareStatement(query);
            pstmt.setString(1,ct.getName());
             
        return pstmt.execute();
    }

    @Override
    public List<Object> findAll() throws SQLException {
        List<Object>ObjectList = new ArrayList<>();
//            List<Catalog>CatalogList = new ArrayList<>();
////        (List<Object>)(Catalog) Objectlist;
//        List<Catalog> CatalogList = (List<Catalog>)(List<Object>);
        String query = "select * from catalog where status != 0";
        Connection connection = ConnectHelper.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(query);
       
        while(rs.next()){
        Catalog ct = new Catalog(rs.getInt("id"), rs.getString("name"), rs.getInt("status"));
//        CatalogList.add(ct);
        ObjectList.add(ct);
//        List<A> a = getListOfA();

//    ObjectList.addAll(CatalogList);
        }
        return ObjectList;
    }
    public List<Catalog> search(String name) throws SQLException {
        List<Catalog>CatalogList = new ArrayList<>();
//            List<Catalog>CatalogList = new ArrayList<>();
////        (List<Object>)(Catalog) Objectlist;
//        List<Catalog> CatalogList = (List<Catalog>)(List<Object>);
        String query = "select * from catalog where status != 0 and name like '%"+name+"%'";
        Connection connection = ConnectHelper.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(query);
       
        while(rs.next()){
        Catalog ct = new Catalog(rs.getInt("id"), rs.getString("name"), rs.getInt("status"));
//        CatalogList.add(ct);
        CatalogList.add(ct);
//        List<A> a = getListOfA();

//    ObjectList.addAll(CatalogList);
        }
        return CatalogList;
    }

    @Override
    public boolean update(Object ob) throws SQLException {
        Catalog ct = new Catalog();
        ct = (Catalog) ob;
        String query = "update catalog set name = ? where id = "+ct.getId();
    Connection connection = ConnectHelper.getConnection();
        PreparedStatement pstmt = connection.prepareStatement(query);
        pstmt.setString(1, ct.getName());
        return pstmt.execute();
    }

    @Override
    public boolean delete(Object ob) throws SQLException {
       Catalog ct = new Catalog();
        ct = (Catalog) ob;
        String query = "update catalog set status = 0 where id = "+ct.getId();
    Connection connection = ConnectHelper.getConnection();
        PreparedStatement pstmt = connection.prepareStatement(query);

        return pstmt.execute();
    }
    
}
