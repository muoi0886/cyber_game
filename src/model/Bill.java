/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author lexuanmuoi
 */
public class Bill {
    public int id, guestId,deviceId,saleId, amount,timePlay, status, createBy, updateBy;
    public String guestName,deviceName;
    public Bill(){};
    public Bill(int id,int guestId,int deviceId,int saleId,int amount,int timePlay,int status,int createBy,int updateBy){
    this.id = id;
    this.guestId = guestId;
    this.deviceId = deviceId;
    this.saleId = saleId;
    this.amount = amount;
    this.timePlay = timePlay;
    this.status = status;
    this.createBy = createBy;
    this.updateBy = updateBy;
    };
    public Bill(int id,int guestId,int deviceId,int amount,int timePlay, int status){
    this.id = id;
    this.guestId = guestId;
    this.deviceId = deviceId;
   this.status = status;
    this.amount = amount;
    this.timePlay = timePlay;
   
    };
    public Bill(int id,String guestName,String deviceName,int amount,int timePlay, int status){
    this.id = id;
    this.guestName = guestName;
    this.deviceName = deviceName;
   this.status = status;
    this.amount = amount;
    this.timePlay = timePlay;
   
    };
    public String getGuestName(){
        return this.guestName;
    };
    public void setGuestName(String guestName){
        this.guestName = guestName;
    };
    public int getId(){
        return this.id;
    }
    public void setId(int id){
        this.id = id;
    }
    public int getGuestId(){
        return this.guestId;
    }
    public void setGuestId(int guestId){
        this.guestId = guestId;
    }
    public int getDeviceId(){
        return this.deviceId;
    }
    public void setDeviceId(int deviceId){
        this.deviceId = deviceId;
    }
    public int getSaleId(){
        return this.saleId;
    }
    public void settSaleId(int saleId){
        this.saleId = saleId;
    }
    public int getAmount(){
        return this.amount;
    }
    public void setAmount(int amount){
        this.amount = amount;
    }
    public int getTimePlay(){
        return this.timePlay;
    }
    public void setTimePlay(int timePlay){
        this.timePlay = timePlay;
    }
    public int getStatus(){
        return this.status ;
    }
    public void setStatus(int status){
        this.status = status;
    }
    public int getCreateBy(){
        return this.createBy ;
    }
    public void setCreateBy(int createBy){
        this.createBy = createBy;
    }
    public int getUpdateBy(){
        return this.updateBy ;
    }
    public void setUpdate(int updateBy){
        this.updateBy = updateBy;
    }
}
