/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author lexuanmuoi
 */
public class DeviceComputer {
    private String cpu, ram, vga, psu, ssd;
    private int id, status;
    public DeviceComputer() {
    }
    
    public DeviceComputer(int id, String cpu, String ram, String vga, 
            String psu, String ssd, int status) {
        this.id = id;
        this.cpu = cpu;
        this.ram = ram;
        this.vga = vga;
        this.psu = psu;
        this.ssd = ssd;
        this.status = status;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return this.id;
    }
    
    public void setCpu(String cpu) {
        this.cpu = cpu;
    }
    
    public String getCpu() {
        return this.cpu;
    }
    
    public void setRam(String ram) {
        this.ram = ram;
    }
    
    public String getRam() {
        return this.ram;
    }
    
    public void setVga(String vga) {
        this.vga = vga;
    }
    
    public String getVga() {
        return this.vga;
    }
    
    public void setPsu(String psu) {
        this.psu = psu;
    }
    
    public String getPsu() {
        return this.psu;
    }
    
    public void setSsd(String ssd) {
        this.ssd= ssd;
    }
    
    public String getSsd() {
        return this.ssd;
    }
    
    public void setStatus(int status) {
        this.status = status;
    }
    
    public int getStatus() {
        return this.status;
    }
}
