/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author lexuanmuoi
 */
public class BillFood extends Bill{
    public int billId, foodId, qty;

    public BillFood() {
    }

    public BillFood(int id,int billId, int foodId, int qty, int createBy, int updateBy) {
        this.id = id;
        this.billId = billId;
        this.foodId = foodId;
        this.qty = qty;
        this.createBy = createBy;
        this.updateBy = updateBy;
    }
    public int getBillId(){
        return this.billId;
    }
    public void setBillId(int billId){
        this.billId = billId;
    }
    public int getFoodId(){
        return this.foodId;
    }
    public void setFoodId(int foodId){
        this.foodId = foodId;
    }
    public int getQty(){
        return this.qty;
    }
    public void setQty(int qty){
        this.qty = qty;
    }
}
