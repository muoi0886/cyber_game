/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author lexuanmuoi
 */
public class Food {
    public int id,status,catalog_id,price;
    String name;
    public Food(){};
    public Food(int id, String name, int catalog_id, int price, int status){
    this.id = id;
    this.name = name;
    this.catalog_id = catalog_id;
    this.price = price;
    this.status = status;
};
     public Food(int id, String name, int price){
    this.id = id;
    this.name = name;
    this.price = price;
     }
    public int getId(){
        return this.id;
    }
    public void setId(int id){
        this.id = id;
    }
    public String getName(){
        return this.name;
    }
    public void setName(String name){
        this.name = name;
    }
    public int getStatus(){
        return this.status ;
    }
    public void setStatus(int status){
        this.status = status;
    }
    public int getCatalog(){
        return this.catalog_id;
    }
    public void setCatalog(int catalog_id){
        this.catalog_id = catalog_id;
    }
    public int getPrice(){
        return this.price ;
    }
    public void setPrice(int price){
        this.price = price;
    }

}
