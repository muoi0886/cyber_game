/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author lexuanmuoi
 */
public class Device {
    private String name;
    private int id,catalog_id, status;
    
    public Device() {
    }
    
    public Device(int id, String name, int catalog_id, int status) {
        this.id = id;
        this.name = name;
        this.catalog_id = catalog_id;
        this.status = status;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return this.id;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setCatalog(int catalog_id) {
        this.catalog_id = catalog_id;
    }
    
    public int getCatalog() {
        return this.catalog_id;
    }
    
    public void setStatus(int status) {
        this.status = status;
    }
    
    public int getStatus() {
        return this.status;
    }
}
