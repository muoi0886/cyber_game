/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author lexuanmuoi
 */
public class Human {
    public String user, password, name, phone, address, cmt, status;
    public int id, createBy, updateBy;
    public Human(){
        
    }

    public Human (int id, String user, String password, String name, String phone, String address, String cmt, String status, int createBy, int updateBy){
        this.id = id;
        this.user = user;
        this.password = password;
        this.name = name;
        this.phone = phone;
        this.address = address;
        this.cmt = cmt;
        this.status = status;
        this.createBy = createBy;
        this.updateBy = updateBy;

    }
    public int getId(){
        return this.id;
    }
    public void setId(int id){
        this.id = id;
    }
    public String getUser(){
        return this.user;
    }
    public void setUser(String user){
        this.user = user;
    }
    public String getPassword(){
        return this.password;
    }
    public void setPassword(String password){
        this.password = password;
    }
    public String getName(){
        return this.name;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getPhone(){
        return this.phone;
    }
    public void setPhone(String phone){
        this.phone = phone;
    }
    public String getAddress(){
        return this.address;
    }
    public void setAddress(String address){
        this.address = address;
    }
    public String getCMT(){
        return this.cmt;
    }
    public void setCMT(String cmt){
        this.cmt = cmt;
    }
    public String getStatus(){
        return this.status;
    }
    public void setStatus(String status){
        this.status = status;
    }
    public int getCreateBy(){
        return this.createBy ;
    }
    public void setCreateBy(int createBy){
        this.createBy = createBy;
    }
    public int getUpdateBy(){
        return this.updateBy ;
    }
    public void setUpdate(int updateBy){
        this.updateBy = updateBy;
    }

    
    
}
