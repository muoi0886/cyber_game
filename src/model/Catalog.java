/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author lexuanmuoi
 */
public class Catalog {
    public int id,status;
    String name;
    public Catalog(){};
    public Catalog(int id, String name,int status){
    this.id = id;
    this.name = name;
    this.status = status;

};
    public int getId(){
        return this.id;
    }
    public void setId(int id){
        this.id = id;
    }
    public String getName(){
        return this.name;
    }
    public void setName(String name){
        this.name = name;
    }
    public int getStatus(){
        return this.status ;
    }
    public void setStatus(int status){
        this.status = status;
    }

}
