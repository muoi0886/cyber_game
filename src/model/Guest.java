/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author lexuanmuoi
 */
public class Guest extends Human {
    public int catalogId, money;
    public Guest(){
    
    }
    public Guest(int id, String user, String password, String name,String phone, 
            String cmt, int catalogId){
        this.id = id;
        this.user = user;
        this.password = password;
        this.name = name;
        this.phone = phone;
        this.cmt = cmt;
        this.catalogId = catalogId;
    }
    public int getCatalogId(){
        return this.catalogId;
    }
    public void setCatalogId(int catalogId){
        this.catalogId = catalogId;
    }
    public int getMoney(){
        return this.money;
    }
    public void setMoney(int money){
        this.money = money;
    }
    
}
