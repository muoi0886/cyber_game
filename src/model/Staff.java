/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author lexuanmuoi
 */
public class Staff extends Human {
    String address;
    public Staff(){
        
    }
    public Staff(int id, String user, String password, String name,String phone,
            String address,String cmt){
        this.id = id;
        this.user = user;
        this.password = password;
        this.name = name;
        this.phone = phone;
        this.address = address;
        this.cmt = cmt;
    }
    
    public void setAddress(String address){
        this.address = address;
    }

    public String getAddress() {
        return this.address;
    }
}
